

$(document).ready(function(){
  $("button").css("display", "none");  
  $('.photo-container').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.photo-bar'
      });
      $('.photo-bar').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.photo-container',
        dots: true,
        centerMode: true,
        focusOnSelect: true
      });
    });